import functions as f
import logging
import argparse
import os

env_versions_level = 'apps'
env_versions_path = 'env_versions'
# env_versions_repo = 'ssh://git@bitbucket.oportun.com:7999/cloud/env_versions.git'
env_versions_repo = 'ssh://IsraelGlez@bitbucket.org/caroherrera/env_versions.git'
appdist_path = 'appdist'
appdist_repo = 'ssh://git@bitbucket.oportun.com:7999/ap/appdist.git'
temp_file = 'tmp.json'
log_file = 'update_json.log'

home = os.getcwd()
logging.basicConfig(level=logging.INFO, filename=home + '/' + log_file,
                    format='%(asctime)s :: %(levelname)s :: %(message)s')


# this scrip needs python module gitpython to be installed to work properly
# it also needs access to git repo env_versions (r/w) and appdist (r)

def arguments():
    """takes the arguments from the command line and pass them to the script to work properly"""
    parser = argparse.ArgumentParser(description='This script will update the json file used for apps deployments, \
    you need to pass the application and build number to update the json file. \
    i.e. python3 update_json.py --source_environment qa --source_instance 1 --destination_environment stage \
    --destination_instance 1 or python3 update_json.py --app api-loan-app --build b4300 --environment qa --instance 1 \
    NOTE: all flags are needed in each command')
    parser.add_argument('-se',
                        '--source_environment',
                        type=str,
                        help='add the source environment to update json file into destination environment')
    parser.add_argument('-de',
                        '--destination_environment',
                        type=str,
                        help='add the destination environment to update json file into source environment')
    parser.add_argument('-di',
                        '--destination_instance',
                        type=str,
                        help='the environment to be used to update app/build')
    parser.add_argument('-si',
                        '--source_instance',
                        type=str,
                        help='the instance number to search the json file')
    parser.add_argument('-a',
                        '--app',
                        type=str,
                        help='add the application name to update search in json file')
    parser.add_argument('-b',
                        '--build',
                        type=str,
                        help='add the build number to update in json file')
    parser.add_argument('-e',
                        '--environment',
                        type=str,
                        help='the environment to be used to update app/build')
    parser.add_argument('-i',
                        '--instance',
                        type=str,
                        help='the instance number to search the json file')
    args = parser.parse_args()

    main(args)


def main(args):
    if args.source_environment is not None and args.source_instance is not None \
            and args.destination_environment is not None and args.destination_instance is not None:
        source_json_file = args.source_environment + '-' + args.source_instance + '.json'
        destination_json_file = args.destination_environment + '-' + args.destination_instance + '.json'
        f.git_clone(env_versions_path, env_versions_repo)
        f.git_clone(appdist_path, appdist_repo)
        dbmigrate_data = f.get_appdist_changes(home, appdist_path)
        source_data = f.read_json(source_json_file, env_versions_path, home)
        new_data = f.update_values(dbmigrate_data, source_data, args)
        f.write_json(new_data, destination_json_file, temp_file, env_versions_path, home, args)
        f.git_push(destination_json_file, args, env_versions_path, home)
    elif args.app is not None and args.build is not None and args.environment is not None and args.instance is not None:
        source_json_file = args.environment + '-' + args.instance + '.json'
        f.git_clone(env_versions_path, env_versions_repo)
        data = f.read_json(source_json_file, env_versions_path, home)
        f.change_build(args.app, args.build, data, env_versions_level)
        f.write_json(data, source_json_file, temp_file, env_versions_path, home)
        f.git_push(source_json_file, args, env_versions_path, home)
    else:
        print(f'please read documentation: python3 update_json.py --help')
        exit(1)


if __name__ == '__main__':
    arguments()
