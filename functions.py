from git import Repo
import json
import os
import logging
import datetime
import time


log_file = 'update_json.log'
home_path = os.getcwd()
logging.basicConfig(level=logging.INFO, filename=home_path + '/' + log_file,
                    format='%(asctime)s :: %(levelname)s :: %(message)s')


def git_clone(git_path, git_repo):
    """Checks if repo was already coned if not it will run the cloning task, if it's already there will pull
    from the repo passed in"""

    if not os.path.isdir(git_path):
        logging.info(Repo.clone_from(git_repo, git_path))
    else:
        repo = Repo(git_path)
        logging.info(repo.remotes.origin.pull())


def git_push(json_file, args, git_path, home):
    """this will determine what kind of update is happening to create the correct commit message, if we are updating
    only one app or if we are promoting the json file to another environment after that will trigger the push_it
    function"""

    os.chdir(home)
    if args.app is not None and args.build is not None:
        commit_message = 'Updating ' + json_file + ' ' + args.app.lower() + ' ' + args.build
        push_it(commit_message, git_path, home)
    elif args.source_environment is not None:
        commit_message = 'Updating ' + json_file + ' from' + args.source_environment.lower()
        push_it(commit_message, git_path, home)
    else:
        logging.info("something went wrong, review git_push function")
        exit(1)


def push_it(commit_message, git_path, home):
    """this will push all the changes to master branch"""

    os.chdir(git_path)
    try:
        repo = Repo()
        repo.git.add(u=True)
        repo.index.commit(commit_message)
        origin = repo.remote(name='origin')
        origin.push()
        logging.info('Pushing changes to ' + git_path)
    except:
        logging.error(print("Something went wrong while pushing code"))
    os.chdir(home)


def read_json(json_file, git_path, home):
    """Reads the file to get the information into a variable, so we can change the old value to the new build version"""

    try:
        os.chdir(git_path)
        with open(json_file, "r") as file:
            data = json.load(file)
    except FileNotFoundError:
        logging.error(print('Something went wrong with file name, please review it'))
        exit(1)
    os.chdir(home)
    return data


def change_build(application, build, data, level):
    """Changes the value of the old application value into the new build number passed by args, it checks if it's
    different of the current value if false it will throw a message mentioning is the same build and exits execution"""

    changed = False
    for key, value in data[level].items():
        if application == key and value != build:
            data[level][key] = build
            logging.info('changing name/value on ' + application + ' ' + build)
            changed = True
    if not changed:
        logging.info(print('build number is not different as current one, exiting...'))
        exit(0)


def write_json(data, json_file, temp_file, path, home, args):
    """takes the new values and write it into a new tmp file, after that the original file is replaced with the newly
    created file"""

    os.chdir(path)
    with open(temp_file, 'w') as tmp_file:
        json.dump(data, tmp_file, indent=2, sort_keys=True)
    logging.info(f"saving changes in {temp_file} and renaming file as {json_file}")
    os.rename(temp_file, json_file)

    if args.destination_environment == 'prod':
        json_file = args.destination_environment + '-2.json'
        data['meta']['target_env'] = 'PROD2'
        with open(temp_file, 'w') as tmp_file:
            json.dump(data, tmp_file, indent=2, sort_keys=True)
        logging.info(f"saving changes in {temp_file} and renaming file as {json_file}")
        os.rename(temp_file, json_file)
        os.chdir(home)
    os.chdir(home)


def get_appdist_changes(home, dir_path):
    """this funtion will read appdist repo, then will create 3 temporal files to work with, appdist.sh, migrate and
    bootstrap, apdist.sh will contain a shell script that will search for the last Release_v##.## tag applied to do a
    git diff to get what needs to be updated in dbmigrate and will save those values into migrate and bootstrap files,
    those files are cleaned removing the \n character. after this all files are removed to keep clean the directory
    this is not writing nothing into appdist repo"""

    files = ['migrate', 'bootstrap', 'appdist.sh']
    os.chdir(dir_path)
    migrate_lines = {'migrate': [], 'bootstrap': []}
    code = ["#!/bin/bash\n", "cd " + home + "/" + dir_path + "\n",
            "export APPDIST_RELEASE=`git tag --list 'Release*' | tail -1`\n",
            # "export APPDIST_RELEASE=Release_v20.41\n",
            "git diff --name-only ${APPDIST_RELEASE}..HEAD | egrep -e \"^db/bootstrap\" | awk -F'/' '{print $3}' | \
            sort -u > bootstrap \n",
            "git diff --name-only ${APPDIST_RELEASE}..HEAD | egrep -e \"^db/migrate\" | egrep _up | \
            egrep -v 010_up_drop_offer_bureau_name.sql | egrep -v 004_up_loan_term.sql | awk -F'/' '{print $3}' | \
            sort -u > migrate \n",
            "cd " + home + '\n']

    with open(dir_path + '.sh', 'w') as file:
        file.writelines(code)

    os.chmod(home + '/' + dir_path + '/' + dir_path + '.sh', 0o755)
    os.system('./' + dir_path + '.sh')

    with open(files[0], 'r') as file:
        migrate_lines[files[0]] = file.readlines()
    with open(files[1], 'r') as file:
        migrate_lines[files[1]] = file.readlines()

    for key in migrate_lines:
        i = 0
        for value in migrate_lines[key]:
            migrate_lines[key][i] = (value.replace('\n', ''))
            i += 1

    for file in files:
        os.remove(file)
        logging.info(f"file {file} was removed...")
    os.chdir(home)
    return migrate_lines


def update_values(data, source, args):
    """it overwrite the read values from the json file to memory to create the new json file promoted from another
    environment"""

    if args.destination_environment.lower() == 'prod':
        source['meta']['release_name'] = 'Release_v' + str(time.strftime("%y", time.localtime())) + '.' + \
                                         str(datetime.date.today().isocalendar()[1])
        source['meta']['target_env'] = (str(args.destination_environment).upper() + str(args.destination_instance))
    elif args.destination_environment.lower() == 'stage':
        source['meta']['release_name'] = 'RC_v' + str(time.strftime("%y", time.localtime())) + '.' + \
                                         str(datetime.date.today().isocalendar()[1])
        source['meta']['target_env'] = (str(args.destination_environment).upper() + '-' +
                                        str(args.destination_instance))
    elif args.destination_environment.lower() == 'qa':
        source['meta']['release_name'] = str(datetime.date.today())
        source['meta']['target_env'] = (str(args.destination_environment).upper() + '-' +
                                        str(args.destination_instance))
    else:
        source['meta']['release_name'] = str(datetime.date.today())
        source['meta']['target_env'] = (str(args.destination_environment).upper() + '-' +
                                        str(args.destination_instance))

    new_data = {'apps': source['apps'], 'db': data, 'meta': source['meta']}
    logging.info(f"new values: {new_data}")
    return new_data
